<?php

	global $lg_tinymce_custom;
	
		$lg_tinymce_custom = array(
	    'title' => 'Custom',
	    'items' =>  array(
	    	array(
	    		'title' => 'Titles',
	    		'items' => array(
					array(
						'title' => 'Title',
					    'selector' => 'p, h1, h2, h3, h4, h5, h6',
					    'classes' => 'title'
					),
					array(
						'title' => 'Subtitle',
					    'selector' => 'p, h1, h2, h3, h4, h5, h6',
					    'classes' => 'subtitle'
					),
	    		)
	    	),
	    	array(
	    		'title' => 'Display',
	    		'items' => array(
	    			array(
						'title' => 'None to lg',
					    'selector' => 'p, h1, h2, h3, h4, h5, h6, img',
					    'classes' => 'd-none d-lg-block'
					),
	    		)
	    	)
	    )
	)

?>