<?php
use Longevity\Framework\Helper;
add_filter('widget_text', 'do_shortcode');
add_filter('wp_nav_menu_items', 'add_nav_box', 10, 2);
function add_nav_box($items, $args){
	$box_image = get_field("box_image", "options");
	$box_blurb = get_field("box_blurb", "options");
	ob_start();
	?>
		<?php if( $args->theme_location == 'top-nav' ): ?>
				<li class="nav-box d-md-none">
        		<div class="box-img">
        		<img src="<?php echo $box_image["url"]; ?>" alt="<?php echo $box_image["alt"]; ?>">
        	    </div>
        		<div class="box-blurb">
        		<?php echo $box_blurb; ?>
        		</div>
        		</li>
        <?php endif; ?>		
	<?php
	$return = ob_get_clean();
	$items .= $return;
    return $items;
}

add_action("wp_body_end", "mailchimp_signup");

function mailchimp_signup() { 
get_template_part('/templates/template-parts/page/video_tours');
get_template_part('/templates/template-parts/page/get_help_today');
get_template_part('/templates/template-parts/page/mailchimp_signup');

}


?>