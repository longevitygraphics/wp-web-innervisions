<?php

/* BEGIN CSS FILE */

global $lg_styles, $lg_scripts;

$lg_styles = [
	(object) array(
		"handle" => "lg-fonts",
		"src" => 'https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i',
		"dependencies" => [],
		"version" => false
	),
	(object) array(
		"handle" => "lg-style",
		"src" => get_stylesheet_directory_uri() . '/assets/dist/css/style.min.css',
		"dependencies" => [],
		"version" => filemtime(get_stylesheet_directory() . '/assets/dist/css/style.min.css')
	)
];

/* END CSS FILE */

/* BEGIN JS FILE */

$lg_scripts = [
	(object) array(
		"handle" => "lg-script",
		"src" => get_stylesheet_directory_uri() . "/assets/dist/js/script.min.js",
		"dependencies" => array('jquery'),
		"version" => filemtime(get_stylesheet_directory() . "/assets/dist/js/script.min.js")
	)
];
/* END JS FILE */

function lg_enqueue_styles_scripts() {

	global $lg_styles, $lg_scripts;

	/* INCLUDE MAIN */

	if($lg_styles && is_array($lg_styles)){
		foreach ($lg_styles as $key => $style) {

			wp_enqueue_style(
				$style->handle,
				$style->src,
				$style->dependencies,
				$style->version
			);
		}
	}

	if($lg_scripts && is_array($lg_scripts)){
		foreach ($lg_scripts as $key => $script) {
			wp_enqueue_script(
				$script->handle,
				$script->src,
				$script->dependencies,
				$script->version
			);

			wp_enqueue_script( $script->handle );
		}
	}

	/* END INCLUDE MAIN */

}

add_action( 'wp_enqueue_scripts', 'lg_enqueue_styles_scripts' );


if($lg_styles && is_array($lg_styles)){
	foreach ($lg_styles as $key => $style) {
		add_editor_style($style->src);
	}
}

/*Remove jquery migrate js*/
function remove_jquery_migrate($scripts)
{
	if (!is_admin() && isset($scripts->registered['jquery'])) {
		$script = $scripts->registered['jquery'];

		if ($script->deps) { // Check whether the script has any dependencies
			$script->deps = array_diff($script->deps, array(
				'jquery-migrate'
			));
		}
	}
}
add_action('wp_default_scripts', 'remove_jquery_migrate');

//Remove Gutenburg css
add_action( 'wp_print_styles', 'wps_deregister_gutenberg_styles', 100 );
function wps_deregister_gutenberg_styles() {
	wp_dequeue_style( 'wp-block-library' );
}

// dequeue stylesheets and scripts conditionally
// function lg_conditional_scripts_styles() {
//     //get current url
//     global $wp;
//     $current_url = home_url( $wp->request );
//     //check if page is not /contact
//     if(!strstr( $current_url, '/contact')){  
//         wp_dequeue_script("lg-map");     
// 	}
// 	wp_dequeue_style('sb-font-awesome');
// }
// add_action('wp_enqueue_scripts', 'lg_conditional_scripts_styles', 100);

?>