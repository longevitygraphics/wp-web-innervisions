// Window Scroll Handler

(function($) {
    $(window).on("scroll", function() {
        if ($(window).width() > 768) {
            var breakpoint = 300;
            var siteheader = $("#site-header");

            if (
                $(window).scrollTop() >= breakpoint &&
                !siteheader.hasClass("scrolled")
            ) {
                siteheader.addClass("scrolled");
            } else if (
                $(window).scrollTop() <= breakpoint &&
                siteheader.hasClass("scrolled")
            ) {
                siteheader.removeClass("scrolled");
            }
        }
    });
})(jQuery);
