<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>
	<?php do_action('wp_content_bottom'); ?>
	</div>
	
	<?php do_action('wp_body_end'); ?>

	<?php $lg_option_footer_site_legal = get_option('lg_option_footer_site_legal'); ?>

	<footer id="site-footer">
		<div id="site-footer-main" class="clearfix py-3 ">
			<div class="container">
			<div class="row d-flex align-items-center">
				<div class="site-footer-alpha col-lg-4 text-center text-lg-left">

					<?php $alpha_image = get_field("footer_alpha_image", "options"); ?>
					<?php $alpha_content = get_field("footer_alpha_blurb", "options"); ?>
					
					<div class="alpha-mobile d-block d-lg-none">
						<img src="<?php echo get_option("lg_option_logo_alt") ?>" alt="<?php echo get_bloginfo ($show = 'name') ?>">
					</div>

					<div class="alpha-md-lg d-none d-lg-flex flex-row">	
						<div class="alpha-image">
	        				<img src="<?php echo $alpha_image["url"]; ?>" alt="<?php echo $alpha_image["alt"]; ?>">
	        	    	</div>
	        			<div class="alpha-content">
	        				<?php echo $alpha_content; ?>
	        			</div>
					</div>
				
				</div>
				<div class="site-footer-bravo col-lg-4"><?php dynamic_sidebar('footer-bravo'); ?>
					<div>&copy; <?php echo date("Y") ?> <a class="text-white" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a> All Rights Reserved</div>
				</div>
				<div class="site-footer-charlie col-lg-4 text-center"><?php dynamic_sidebar('footer-charlie'); ?></div>
			</div>
			
		</div>
		</div>

		<?php if(!$lg_option_footer_site_legal || $lg_option_footer_site_legal == 'enable'): ?>
			<div id="site-legal" class="py-3 px-3 d-flex justify-content-center justify-content-md-between align-items-center flex-wrap">
				<div class="site-info"><?php get_template_part("/templates/template-parts/footer/site-info"); ?></div>
				<div class="site-longevity"> <?php get_template_part("/templates/template-parts/footer/site-footer-longevity"); ?> </div>
			</div>
		<?php endif; ?>

	</footer><!-- #colophon -->

<?php wp_footer(); ?>

</body>
</html>
