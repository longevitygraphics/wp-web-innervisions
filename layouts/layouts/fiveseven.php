<?php 
/**
 * Five Seven Block Layout
 *
 */
?>

<?php

	get_template_part('/layouts/partials/block-settings-start');

?>

<!--------------------------------------------------------------------------------------------------------------------------------->

	<?php 
	
		// Variables Start

		$left_column = get_sub_field('left_column');
		$right_column = get_sub_field('right_column');

		$left_size = get_sub_field('left_size');
		$right_size = get_sub_field('right_size');

		$left_content = $left_column['left_content'];
		$right_content = $right_column['right_content'];

		$left_color = $left_column['left_colour'];
		$right_color = $right_column['right_colour'];

		$left_width_xs = $left_size['left_width_xs'];
		$right_width_xs = $right_size['right_width_xs'];

		$left_width_sm = $left_size['left_width_sm'];
		$right_width_sm = $right_size['right_width_sm'];

		$left_width_md = $left_size['left_width_md'];
		$right_width_md = $right_size['right_width_md'];

		$left_width_lg = $left_size['left_width_lg'];
		$right_width_lg = $right_size['right_width_lg'];

		$left_width_xl = $left_size['left_width_xl'];
		$right_width_xl = $right_size['right_width_xl'];

		// Variables End
	?> 

	<div class="d-flex flexible_text <?php if($container == 'container-wide'){echo 'no-gutters';} ?> row <?php the_sub_field('align_items_vertical'); ?> <?php the_sub_field('align_items_horizontal'); ?>">
		
		<!-- Start Structure Here -->
		<div class="col-xs-<?php echo $left_width_xs; ?> col-sm-<?php echo $left_width_sm?>; col-md-<?php echo $left_width_md; ?> col-lg-<?php echo $left_width_lg; ?> col-xl-<?php echo $left_width_xl; ?> bg-<?php echo $left_color; ?> fs-left pt-5 pb-5">
			<div class="px-1 px-md-4">
				<?php echo $left_content; ?>
			</div>
		</div>

		<div class="col-xs-<?php echo $right_width_xs; ?> col-sm-<?php echo $right_width_sm; ?> col-md-<?php echo $right_width_md; ?> col-lg-<?php echo $right_width_lg; ?> col-xl-<?php echo $right_width_xl; ?> bg-<?php echo $right_color; ?> fs-right pt-5 pb-5">
			<div class="px-1 px-md-4">
				<?php echo $right_content; ?>
			</div>
		</div>
		<!-- End Structure Here -->

	</div>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php 

	get_template_part('/layouts/partials/block-settings-end');

?>
