<?php

	switch ( get_row_layout()) {
		case 'example':
			get_template_part('/layouts/layouts/example');
		break;
		case 'fiveseven':
			get_template_part('/layouts/layouts/fiveseven');
		break;
		case 'mens_sidebar':
			get_template_part('/layouts/layouts/mens-sidebar');
		break;
		case 'womens_sidebar':
			get_template_part('/layouts/layouts/womens-sidebar');
		break;
	}

?>