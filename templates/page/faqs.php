<?php

get_header(); ?>

	<main>

		<?php
			$faqs = get_field('faqs');
		?>

		<div class="container-fluid">
			<div class="row faqs">
				
				<!-- Start Structure Here -->
				<div class="col-lg-5 bg-light-primary fs-left pt-5 pb-5">
					<div class="px-1 px-md-4">
						<?php if($faqs && is_array($faqs)): ?>
							<div class="d-flex flex-column">
							<?php foreach ($faqs as $key => $faq):?>
								<a class="faq-index px-3 py-2" index="<?php echo $key; ?>"><?php echo $faq['question']; ?></a>
							<?php endforeach; ?>
							</div>
						<?php endif; ?>
					</div>
				</div>

				<div class="col-lg-7 bg-white fs-right pt-5 pb-5">
					<div class="px-1 px-md-4">
						<?php if($faqs && is_array($faqs)): ?>
							<div class="d-flex flex-column">
							<?php foreach ($faqs as $key => $faq):?>
								<div class="faq-content fade" index="<?php echo $key; ?>">
									<div class="bg-dark p-3 text-white"><?php echo $faq['question']; ?></div>
									<div class="bg-light-primary p-3"><?php echo $faq['answer']; ?></div>
								</div>
							<?php endforeach; ?>
							</div>
						<?php endif; ?>
					</div>
				</div>
				<!-- End Structure Here -->

			</div>
		</div>

		<script>
			(function($) {
	
			    $(document).ready(function(){

			    	$('.faq-index').on('click', function(){
			    		var index = $(this).attr('index');
			    		$(this).addClass('active').siblings().removeClass('active');
			    		$('.faq-content[index='+index+']').addClass('active').addClass('show').siblings().removeClass('active').removeClass('show');

			    		if($(window).width() <= 991){
			    			$('.fs-right').css('display', 'flex').hide().fadeIn();
			    		}else{
			    			var scrollTo = $('body');
							var settings = {
						        duration: 1000,
						        offset: ($('.site-header').outerHeight()) * -1
						    };
			    			KCollection.headerScrollTo(scrollTo, settings);
			    		}
			    	});


			    	if($(window).width() > 991){
			    		$('.faq-index[index="0"]').click();
			    	}else{
			    		$('.fs-right').on('click', function(){
			    			console.log('dwqdqw');
			    			$(this).fadeOut();
			    		});

			    		$('.fs-right >div').on('click', function(e){
			    			e.stopPropagation();
			    		});
			    	}
			        
			    });

			}(jQuery));
		</script>

	</main>

<?php get_footer(); ?>