<?php
/**
 * Main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 */

get_header(); ?>

	<main>
		<?php
			$feature_image = get_field("image");
			$feature_logo = get_field("slider_logo");
			$feature_title = get_field("slider_title");
			$feature_subtitle = get_field("slider_subtitle");
			$feature_button = get_field("slider_button");
		?>
		<div class="feature-slider d-flex justify-content-center flex-wrap align-items-start" style= "background-image:url(<?php echo $feature_image["url"] ?>);">
			<div class="feature-content d-flex justify-content-center flex-wrap align-items-center text-center flex-column">
				<div class="feature-logo d-none d-lg-block">
					<img src="<?php echo $feature_logo['url']; ?>" alt = "<?php echo $feature_logo['alt']; ?>">
				</div>
				<div class="feature-title">
					<p class = "title"> <?php echo $feature_title ?>
				</div>
				<div class="feature-subtitle">
					<p class = "subtitle"> <?php echo $feature_subtitle ?>
				</div>
				<div class="feature-btn">
					<a class = "btn btn-secondary" href="<?php echo $feature_button['url'] ?>" target="<?php echo $feature_button['target'] ?>"> <?php echo $feature_button['title'] ?> </a>
				</div>
			</div>
		</div>


		<?php flexible_layout(); ?>

	</main>

<?php get_footer(); ?>