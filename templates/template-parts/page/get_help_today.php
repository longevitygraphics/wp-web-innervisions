<div class="site-footer-cta clearfix py-3 container-fluid justify-content-center align-items-center flex-wrap bg-gray d-flex flex-md-column">
    <?php
			$cta_title = get_field("cta_title", "options");
			$cta_link_alpha = get_field("cta_link_alpha" , "options");
			$cta_link_beta = get_field("cta_link_beta", "options");
			$cta_link_gamma = get_field("cta_link_gamma", "options");
		?>
    <div class="cta-title">
        <p class="h1 text-dark">
            <?php echo $cta_title ?>
        </p>
    </div>
    <div class="cta-buttons d-flex justify-content-center flex-column flex-md-row">
        <a href="<?php echo $cta_link_alpha["url"] ?>" class="btn btn-secondary" target="<?php echo $cta_link_alpha["target"] ?>">
            <?php echo $cta_link_alpha["title"] ?></a>
        <a href="<?php echo $cta_link_beta["url"] ?>" class="btn btn-secondary inverse" target="<?php echo $cta_link_beta["target"] ?>">
            <?php echo $cta_link_beta["title"] ?></a>
        <a href="<?php echo $cta_link_gamma["url"] ?>" class="btn btn-secondary" target="<?php echo $cta_link_gamma["target"] ?>">
            <?php echo $cta_link_gamma["title"] ?></a>
    </div>
</div>