<?php if (have_rows("video_repeater", "options")): ?>
<div class="site-footer-video-tours clearfix py-3 container-fluid justify-content-center align-items-center flex-wrap bg-light d-flex flex-md-column">
    <h2 class="h1 text-dark font-weight-light">Take a Tour of Our Recovery Houses</h2>
    <div class="d-flex video-container">
        <?php while (have_rows("video_repeater", "options")): the_row() ?>
        <?php 
                $show_title = get_sub_field("show_video_title");
                $video_title = get_sub_field("video_title");
                $video = get_sub_field("video");
            ?>
        <div class="video-wrapper">
            <div class="video embed-responsive embed-responsive-16by9">
                <?php if ($show_title): ?>
                <h3>
                    <?php echo $video_title ?>
                </h3>
                <?php endif; ?>
                <?php echo $video ?>
            </div>
        </div>
        <?php endwhile; ?>
    </div>
</div>
<?php endif ?>
